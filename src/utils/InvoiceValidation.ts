import joi from "joi";

const InvoiceSchema = joi.object({
  merchantStreetAddress: joi.string().required(),
  merchantName: joi.string().required(),
  merchantEmail: joi.string().email().required(),
  merchantPostcode: joi.string().required(),
  merchantCity: joi.string().required(),
  merchantCountry: joi.string().required(),
  recipientStreetAddress: joi.string().required(),
  recipientName: joi.string().required(),
  recipientEmail: joi.string().email().required(),
  recipientPostcode: joi.string().required(),
  recipientCity: joi.string().required(),
  recipientCountry: joi.string().required(),
  invoiceDate: joi.date(),
  status: joi.string().required(),
  paymentTerms: joi.string().required(),
  projectDescription: joi.string().required(),
  totalPrice: joi.number(),
  itemList: joi.array().items({
    itemName: joi.string(),
    itemQuantity: joi.number(),
    itemPrice: joi.number(),
  }),
});

export default InvoiceSchema;
