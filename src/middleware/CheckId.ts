import { NextFunction, Request, Response } from "express";
import mongoose from "mongoose";

const checkId = (
  req: Request,
  res: Response,
  next: NextFunction,
  val: string
) => {
  const ObjectId = mongoose.Types.ObjectId;
  const isValid = ObjectId.isValid(val);
  if (!isValid) {
    res.status(404).json({
      staus: 404,
      message: "Not Found: Invoice does not exist",
    });
  }
  next();
};

export default checkId;
