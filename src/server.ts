import mongoose from "mongoose";

const uri = process.env.DATABASE?.replace(
  "<password>",
  process.env.PASSWORD as string
);

export default async () => {
  try {
    await mongoose.connect(uri as string, {});
    console.log("DB Connected");
  } catch (error) {
    console.log(error);
  }
};
