import { Schema, model } from "mongoose";

export interface ItemType {
  itemName: string;
  itemQuantity: number;
  itemPrice: number;
  id: any;
}

export interface InvoiceType {
  id?: string;
  merchantName: string;
  merchantEmail: string;
  merchantStreetAddress: string;
  merchantCity: string;
  merchantPostcode: string;
  merchantCountry: string;
  recipientName: string;
  recipientEmail: string;
  recipientStreetAddress: string;
  recipientCity: string;
  recipientPostcode: string;
  recipientCountry: string;
  invoiceDate?: Date;
  status: "Draft" | "Pending" | "Paid";
  paymentTerms: "7 days" | "14 days" | "30 days";
  projectDescription: string;
  totalPrice?: number;
  itemList: ItemType[];
}

const InvoiceSchema = new Schema<InvoiceType>(
  {
    merchantStreetAddress: {
      type: String,
      required: [true, "merchant street name is required"],
      trim: true,
    },
    merchantName: {
      type: String,
      required: [true, "merchant name is required"],
      trim: true,
    },
    merchantEmail: {
      type: String,
      trim: true,
    },
    merchantPostcode: String,
    merchantCity: String,
    merchantCountry: String,
    recipientStreetAddress: {
      type: String,
      required: [true, "recipient street name is required"],
      trim: true,
    },
    recipientName: {
      type: String,
      required: [true, "recipient name is required"],
      trim: true,
    },
    recipientEmail: {
      type: String,
      trim: true,
    },
    recipientPostcode: String,
    recipientCity: String,
    recipientCountry: String,
    invoiceDate: { type: Date, default: new Date() },
    status: {
      enum: ["Draft", "Pending", "Paid"],
      default: "Draft",
      type: String,
    },
    paymentTerms: {
      enum: ["7 days", "14 days", "30 days"],
      default: "14 days",
      type: String,
    },
    projectDescription: String,
    totalPrice: Number,
    itemList: [{ itemName: String, itemPrice: Number, itemQuantity: Number }],
  },
  {
    timestamps: true,
    toJSON: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        ret.invoiceDate = ret.createdAt;
        ret.itemList.forEach((item: any) => {
          item.id = item._id;
          delete item._id;
        });
        delete ret._id;
        delete ret.__v;
        delete ret.createdAt;
      },
    },
  }
);

InvoiceSchema.pre("save", function () {
  const totalPrice = this.itemList.reduce((acc: number, value: ItemType) => {
    return acc + value.itemQuantity * value.itemPrice;
  }, 0);
  this.totalPrice = totalPrice;
});

InvoiceSchema.pre("updateOne", function () {
  const totalPrice = this.itemList.reduce((acc: number, value: ItemType) => {
    return acc + value.itemQuantity * value.itemPrice;
  }, 0);
  this.totalPrice = totalPrice;
});

export default model<InvoiceType>("Invoice", InvoiceSchema);
