import { NextFunction, Response, Request } from "express";
import mongoose from "mongoose";
import createError from "http-errors";
import Invoice, { ItemType } from "../model/Invoice.Model";
import InvoiceSchema from "../utils/InvoiceValidation";

const ObjectId = mongoose.Types.ObjectId;

export const getInvoices = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const invoices = await Invoice.find();
    res.status(200).json({
      message: "success",
      length: invoices.length,
      status: 200,
      invoices,
    });
  } catch (error) {
    next(error);
  }
};

export const getInvoice = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { id } = req.params;
    const invoice = await Invoice.findById(id);
    if (invoice) {
      res.status(200).json({
        message: "success",
        status: 200,
        invoice,
      });
    } else {
      next(createError(404, "Not found"));
    }
  } catch (error) {
    next(error);
  }
};

export const createInvoice = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const result = await InvoiceSchema.validateAsync(req.body);
    if (!result) createError(400, "Invalid invoice data");
    const invoice = new Invoice(result);
    const payload = await invoice.save();
    res.status(201).json({
      status: 201,
      message: "created",
      invoice: payload,
    });
  } catch (error) {
    next(error);
  }
};

export const updateInvoice = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const invoice = await Invoice.findByIdAndUpdate(id, body, { new: true });
    res.status(200).json({
      status: 200,
      message: "updated",
      invoice: invoice,
    });
  } catch (error) {
    next(error);
  }
};

export const addItemsToList = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const invoice = await Invoice.findById(id);
    body.forEach((item: ItemType) => {
      invoice?.itemList?.push(item);
    });
    const data = await invoice?.save();
    res.status(201).json({
      message: "success",
      invoice: data,
    });
  } catch (error) {
    next(error);
  }
};

export const removeItemsFromList = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const invoice = await Invoice.findByIdAndUpdate(
      id,
      {
        $pull: {
          itemList: { _id: body.id },
        },
      },
      { new: true }
    );
    res.status(200).json({
      message: "list updated",
      invoice,
    });
  } catch (error) {
    next(error);
  }
};

export const updateListItem = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { params, body } = req;
    const { id } = params;
    const invoice = await Invoice.findById(id);
    // invoice!.itemList.id(body.id).remove()
  } catch (error) {
    next(error);
  }
};

export const deleteInvoice = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    await Invoice.findByIdAndDelete(req.params.id);
  } catch (error) {
    next(error);
  }
};

export const changeToPaid = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { id } = req.params;
    req.body = { status: "Pending" };
    const invoice = await Invoice.findByIdAndUpdate(id, req.body, {
      new: true,
    });
    res.status(200).json({
      message: "updated",
      invoice,
    });
  } catch (error) {
    next(error);
  }
};
