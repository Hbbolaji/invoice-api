import { Router } from "express";
import {
  addItemsToList,
  createInvoice,
  deleteInvoice,
  getInvoice,
  getInvoices,
  updateInvoice,
  removeItemsFromList,
  changeToPaid,
  updateListItem,
} from "../controllers/InvoiceController";
import checkId from "../middleware/CheckId";

const router = Router();
router.param("id", checkId);
router.route("/").get(getInvoices).post(createInvoice);
router.route("/:id").get(getInvoice).patch(updateInvoice).delete(deleteInvoice);

router.route("/changeToPaid/:id").put(changeToPaid);
router.route("/item/add/:id").put(addItemsToList);
router.route("/item/remove/:id").put(removeItemsFromList);
export default router;
