import express, { NextFunction, Request, Response } from "express";
import cors from "cors";
import createError from "http-errors";
import dotenv from "dotenv";
dotenv.config({ path: "./config.env" });

import InoviceRouter from "./routes/InvoiceRouter";

interface ResponseError extends Error {
  status?: number;
}

import server from "./server";

const app = express();
// middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// route
app.use("/api/invoices", InoviceRouter);

// Error Handling
app.use(async (req: Request, res: Response, next: NextFunction) => {
  next(createError(404, "Not Found"));
});

// Error Handler Response
app.use(
  (err: ResponseError, req: Request, res: Response, next: NextFunction) => {
    const status = err?.status || 500;
    res.status(status as number);
    res.json({
      error: {
        status: status,
        message: err.message,
      },
    });
    next();
  }
);

const port = process.env.PORT || 5050;

server();
app.listen(port, () => {
  console.log(`Server is running`);
});
